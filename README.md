# Death at work

## Description

This project is about listing / estimating the number of fatal injuries at work. This project appeared in a continuation of the publication from the [World Health Organization](https://www.who.int/news/item/16-09-2021-who-ilo-almost-2-million-people-die-from-work-related-causes-each-year) stating that almost 2 million people die from work-related causes each year. 

This module **injuries.py** is composed of 3 parts.
- Data have to be download from different sources.
- Data have to be refined
- Calculation of fatal injuries per country / per sex / per year / per EXIOBASE sector.

Data were sourced from the World Health Organization [World Health Organization](https://www.who.int/news/item/16-09-2021-who-ilo-almost-2-million-people-die-from-work-related-causes-each-year) (Pega et al., 2021) and [Eurostat](https://ec.europa.eu/eurostat/web/main/home) (Publications Office of the European Union, 2013). The WHO data was carefully screened based on specific criteria such as age above 15 years, gender, and fatal injuries only. Eurostat data provided granular information on work-related fatalities, classified by economic activities in the European Community (or NACE Rev.2 (Eurostat, 2008)). The WHO provided aggregate fatality data for 2010 and 2016. The strategy for allocating these deaths across Eurostat categories depended on the countries' geographical location, with different methods applied to European and non-European nations. 

For European nations, fluctuations in fatality numbers within a NACE Rev.2 sector mirrored the changes registered by Eurostat. For non-European countries, fatality figures were proportionally allocated across economic sectors split according to the NACE Rev.2 classification, reflecting the workforce size associated with each economic sector. Due to the scarcity of data for nations within Asia, America, or Africa, we adopted a regional approach, computing fatality ratios over each NACE Rev.2 category for each region by integrating data for available countries over a reference year. For 2010 and 2016, the aggregate fatality figures for nations within these three zones were established. Due to the temporal proximity of both reference years, we postulated a linear trend in the fatality count between these two years. The number of fatalities for a specific country, year, and per NACE Rev.2 activity was then calculated by applying the previously mentioned fatality ratio to the total number of deaths for that nation. Last, we applied the European annual ratios to their total mortality figures for the few countries that could not be classified as European or belonging to one of the aforementioned zones.

The result is a comprehensive database that includes the number of fatalities (expressed in the number of deaths for work-related fatal occupational injuries and Disability-adjusted life years (DALYs), for fatalities associated with occupational exposure to a specific risk factor), detailed at the country, gender, and NACE Rev.2 sector levels from 2008 to 2019, providing insights into work-related fatal injuries across different health effects and geographical regions. 

### Download
In order to set up the final table, we need a certain amount of data.

#### WHO
First, we download the data combined by WHO:

```Python
src_url = url_who()
src_csv = Path("WHO_ILO_BOD_LWH_2021_05.csv")
```
These data are stored in the dataframe called **who**.
We then keep only the one of interest :
- age above 15 years old
- we keep the split by sex (so we remove all data related to the total population)
- we keep the data related to injuries leading to Death
- we keep only the number of death, not the rate or the population attributable fraction.

```Python

who = who[who['age'].str.contains('≥15 years',regex=True)]
who = who[who['sex'].str.contains('Male|Female',regex=True)]
who = who[who['measure'].str.contains('Deaths',regex=True)]
who=who.drop(columns=['Population-attributable fraction (%)','Rate (per 100,000 of population)'])
who = who[who['year'] >1994]

country_code = list(who['country_name_who'])

who.insert(4, 'ISO3', cc_all.convert(names = country_code, to='ISO3'))
who.insert(5, 'EXIO3', cc_all.convert(names = country_code, to='EXIO3'))
who = who[~who['EXIO3'].str.contains('not found',regex=True)]
who=who.drop(columns=['risk_factor','age'])
```
We then end up with the number of death per country and per sex for only 3 years (2000,2010 and 2016).

#### Eurostat
Then, we download data related to fatal accident at work by NACE Rev.2 activity from [Eurostat](https://ec.europa.eu/eurostat/web/main/home):

```Python
src_url2 = "https://ec.europa.eu/eurostat/estat-navtree-portlet-prod/BulkDownloadListing?file=data/hsw_n2_02.tsv.gz"
src_csv2 = Path("hsw_n2_02.tsv")
```

These data are stored in a dataframe called **injuries_fatal**.
The table from Eurostat contains data related to number of death by year, sex and nace activities for european countries.

#### Combining the data
We based our calculation on the values available in **who**. As we stated previously, only 3 years are availables.
We first focus on years 2000 and 2016.

##### 2010 and 2016 
Depending on the country of interest, 2 methods are applied.
For the non european countries we need the workforce associated to each EXIOBASE sector (or Nace Rev. 2 activity). This is found in the table located in the **aux** folder : **aux/table_workforce_by_ISO3.csv**. The data are stored in the dataframe called **workforce**.

**_For non european countries :_**
1. we get the number of death from **who** 
2. fatality figures were proportionally allocated across economic sectors split according to the NACE Rev.2 classification at the european level
```Python
injuries_split_ROW.loc[(injuries_split_ROW['ISO3']==code)&(injuries_split_ROW['EXIOBASE sector']==a)&(injuries_split_ROW['sex']==b)&(injuries_split_ROW['estimate']==c)&(injuries_split_ROW['year']==year),'death']=(float(who_aggregate.loc[who_aggregate.index==(year, code, 'Deaths', c, b),'Number (for DALYs in 1000s)'].to_string(index=False,header=False))*float(injuries_fatal_aggregate_nace[str(year)]['NR', str(a)])/float(injuries_fatal_aggregate_year[str(year)]['NR']))
```   

where 
```Python
injuries_split_ROW.loc[(injuries_split_ROW['ISO3']==code)&(injuries_split_ROW['EXIOBASE sector']==a)&(injuries_split_ROW['sex']==b)&(injuries_split_ROW['estimate']==c)&(injuries_split_ROW['year']==year),'death']
```
is the number of death per country per Nace Rev 2 activity per sex and per year we are calculating and storing in a new dataframe called **injuries_split_ROW**.

```python
who_aggregate.loc[who_aggregate.index==(year, code, 'Deaths', c, b),'Number (DALYs in 1000s)']
```
is the number of death per country per year (2010 or 2016) per sex and per Nace rev2 activity.



**_For european countries :_**
```Python
injuries_split_ROW.loc[(injuries_split_ROW['ISO3']==code)&(injuries_split_ROW['EXIOBASE sector']==a)&(injuries_split_ROW['sex']==b)&(injuries_split_ROW['estimate']==c)&(injuries_split_ROW['year']==year),'death']=round(float(who_aggregate.loc[who_aggregate.index==(year, code, 'Deaths', c,   b),'Number (DALYs in 1000s)'].to_string(index=False,header=False))*float(injuries_fatal.loc[(injuries_fatal['unit']=='NR')&(injuries_fatal['nace_r2']==a)&(injuries_fatal['ISO3']==code),str(year)].to_string(index=False,header=False))/float(injuries_fatal_aggregate.loc[injuries_fatal_aggregate.index==('NR', code),str(year)].to_string(index=False,header=False)))
```

where 
```Python
injuries_split_ROW.loc[(injuries_split_ROW['ISO3']==code)&(injuries_split_ROW['EXIOBASE sector']==a)&(injuries_split_ROW['sex']==b)&(injuries_split_ROW['estimate']==c)&(injuries_split_ROW['year']==year),'death']
```
is the number of death per country per Nace Rev 2 activity per sex and per year we are calculating and storing in a new dataframe called **injuries_split_ROW**.

```python
who_aggregate.loc[who_aggregate.index==(year, code, 'Deaths', c, b),'Number (DALYs in 1000s)']
```
is the number of death per country per year (2010 or 2016) per sex and per Nace rev2 activity.

```Python
injuries_fatal.loc[(injuries_fatal['unit']=='NR')&(injuries_fatal['nace_r2']==a)&(injuries_fatal['ISO3']==code),str(year)]
```
is the number of death per country per sex per year and per Nace Rev2 activity

```Python
injuries_fatal_aggregate.loc[injuries_fatal_aggregate.index==('NR', code),str(year)]
```
is the total number of death per country per sex per year no matter the Nace Rev2 activity.


so if A is the number of death per country per sex per year per EXIOBASE activity found in who,
if B is the death per country per sex per year per EXIOBASE activity found in Eurostat,
if c is the total number of death per country per sex per year found in Eurostat,
if D is the number of death per country per sex per year and per EXIOBASE activity,

**D = A * (B/C)**


##### Years before 2010, between 2011 and 2015 and after 2016 

For the years before 2010, we use the well known values from the WHO database for year 2010. For the years coming after 2010, we choose to take the 2016 values from the WHO database.

**_For european countries :_**
For the years previous to 2010, we keep 2010 as a reference as 2010 is available in the database who.
We need to evaluate what could have been the value of death per country per sex per year and per Nace rev2 activity (called **unknown_who** in the script). We condider this value should vary like the number of death available in the Eurostat database (injuries_fatal):

```Python
unknown_who = round(float(who_aggregate.loc[who_aggregate.index==(2010, code, 'Deaths', c,   b),'Number (DALYs in 1000s)'].to_string(index=False,header=False)) + ((float(injuries_fatal_aggregate.loc[injuries_fatal_aggregate.index==('NR', code),str(year)].to_string(index=False,header=False))-float(injuries_fatal_aggregate.loc[injuries_fatal_aggregate.index==('NR', code),str(2010)].to_string(index=False,header=False)))/(float(injuries_fatal_aggregate.loc[injuries_fatal_aggregate.index==('NR', code),str(2010)].to_string(index=False,header=False)))))
```
So if the number of death increases by 10% from 2009 to 2010 for a country for a sex category in the Eurostat database, we condider that the number of death in the WHO database between 2009 and 2010 should also increase by 10%.
Having an estimate of the death from the WHO database, we follow the same method as previously in order to split the number of death by Nace Rev2 activity :

```Python
injuries_split_ROW.loc[(injuries_split_ROW['ISO3']==code)&(injuries_split_ROW['EXIOBASE sector']==a)&(injuries_split_ROW['sex']==b)&(injuries_split_ROW['estimate']==c)&(injuries_split_ROW['year']==year),'death']=round(unknown_who*float(injuries_fatal.loc[(injuries_fatal['unit']=='NR')&(injuries_fatal['nace_r2']==a)&(injuries_fatal['ISO3']==code),str(year)].to_string(index=False,header=False))/float(injuries_fatal_aggregate.loc[injuries_fatal_aggregate.index==('NR', code),str(year)].to_string(index=False,header=False)))
```


**_For countries belonging to America, Asia or Africa:_**


We know the workforce by country, year, sex, sector: **pop_year**
```python
 pop_year = float(workforce.loc[(workforce.ref_area==code)&(workforce.time==year)&(workforce.sex==b)&(workforce.classif1=='ECO_DETAILS_'+str(a)),'obs_value'].to_string(index=False,header=False))
```

We evaluated the ratio of death per sector (this is accessible in "aux/ratio.csv") in the corresponding region (America, Asia or Africa) : **ratio_region_ISIC**
```python
ratio_region_ISIC = float(ratio.loc[(ratio['Sub-region Name']==region_broad),'ECO_ISIC4_'+str(a)].to_string(index=False,header=False))
```
We can then evaluate the number of deaths in this sector : 

death_year = (float(pop_year * ratio_region_ISIC))
```python
injuries_split_ROW.loc[(injuries_split_ROW['ISO3']==code)&(injuries_split_ROW['EXIOBASE sector']==a)&(injuries_split_ROW['sex']==b)&(injuries_split_ROW['estimate']==c)&(injuries_split_ROW['year']==year),'death']=death_year
```




**_For non european countries and country not belonging to Asia, America or Africa :_**

2010 and 2016 are 2 well knowned years. 
As stated in the introduction, we postulated a linear trend in the fatality count between these two years

ratio2010 and ratio2016 are simply pourcentage of deaths amoung the working population.

```python
ratio2010=float(100*(death2010/pop2010))
ratio2016=float(100*(death2016/pop2016))
```
We consider the ratio of death amoung a sector, at the regional level to be constant over the years.
So, knowing the workforce per sector, year, sex, country, we can evaluate the number of death.

```python
delta_ratio=float((ratio2016-ratio2010)/6.0)
pop_year = float(workforce.loc[(workforce.ref_area==code)&(workforce.time==year)&(workforce.sex==b)&(workforce.classif1=='ECO_DETAILS_'+str(a)),'obs_value'].to_string(index=False,header=False))

ratio_year = float((year-2010)*delta_ratio+ratio2010)
death_year = (float(pop_year * ratio_year/100))
if death_year > 0.0:
    injuries_split_ROW.loc[(injuries_split_ROW['ISO3']==code)&(injuries_split_ROW['EXIOBASE sector']==a)&(injuries_split_ROW['sex']==b)&(injuries_split_ROW['estimate']==c)&(injuries_split_ROW['year']==year),'death']=death_year
```


### injuries_split_ROW

**injuries_split_ROW** is the whole dataframe where, for each ISO3 code available in the who dataframe, we have for years 2008 to 2019, the number of death for each sex and each EXIOBASE sector. 


                                




