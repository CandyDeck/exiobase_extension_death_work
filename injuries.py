from download import download_data
from pathlib import Path
import country_converter as coco
import re
import pandas as pd 

from get_url import url_who
from injuries_calculation import non_fatal_calculation
from injuries_calculation import fatal_calculation
from injuries_calculation import who_calculation


storage_root = Path(".").absolute()
download_path = storage_root / "download"
data_path = storage_root / "data"

'''
URL of tables
'''

src_url = url_who()
#src_url = 'https://who-ilo-joint-estimates.shinyapps.io/OccupationalBurdenOfDisease/_w_9ae0c5cd/session/e5294b9a68c532a1fecbab0acfbfa81b/download/cmd_export_data?w=9ae0c5cd'
src_csv = Path("WHO_ILO_BOD_LWH_2021_05.csv")

src_url2 = "https://ec.europa.eu/eurostat/estat-navtree-portlet-prod/BulkDownloadListing?file=data/hsw_n2_02.tsv.gz"
src_csv2 = Path("hsw_n2_02.tsv")

src_url3 = "https://www.ilo.org/ilostat-files/WEB_bulk_download/indicator/INJ_FATL_ECO_NB_A.csv.gz"
src_csv3 = Path("INJ_FATL_ECO_NB_A.csv")

#src_url3 = "https://www.ilo.org/ilostat-files/WEB_bulk_download/indicator/INJ_NFTL_ECO_NB_A.csv.gz"
#src_csv3 = Path("INJ_NFTL_ECO_NB_A.csv")



'''
Download compressed files, save them in "download" folder
uncompressed files and save them in "data" folder
'''
download_data(src_url,src_csv,src_url2,src_csv2,src_url3,src_csv3,data_path)


'''
Calculation of  injuries
'''

injuries_fatal = fatal_calculation(data_path,src_csv2)
injuries_fatal = injuries_fatal.replace({':':''}, regex=True)
injuries_fatal[injuries_fatal.columns[3:]] = injuries_fatal[injuries_fatal.columns[3:]].replace({'[a-zA-Z]':''},regex=True)
injuries_fatal= injuries_fatal[injuries_fatal['nace_r2'].map(len) < 2]

injuries_fatal.columns = injuries_fatal.columns.str.strip()

for i in range(3,15):
    injuries_fatal[injuries_fatal.columns[i]]=pd.to_numeric(injuries_fatal[injuries_fatal.columns[i]],errors = 'coerce')
    injuries_fatal[injuries_fatal.columns[i]] = injuries_fatal[injuries_fatal.columns[i]].fillna(0)

injuries_fatal.loc[(injuries_fatal['geo\\time']=='UK'),['geo\\time']]='GB'
injuries_fatal.loc[(injuries_fatal['geo\\time']=='EL'),['geo\\time']]='GR'

injuries_fatal = injuries_fatal[~injuries_fatal['geo\\time'].str.contains('EU15|EU28|EU27_2007|EU27_2020',regex=True)]
geo_code = list(injuries_fatal['geo\\time'])

cc_all = coco.CountryConverter(include_obsolete=True)
cc = coco.CountryConverter()

iso3_code = injuries_fatal.insert(3, 'ISO3', cc.convert(names = geo_code,src='ISO2', to='ISO3'))

injuries_fatal_aggregate=injuries_fatal.groupby(['unit','ISO3'],axis=0).sum()
injuries_fatal_aggregate_nace=injuries_fatal.groupby(['unit','nace_r2'],axis=0).sum()
injuries_fatal_aggregate_year=injuries_fatal.groupby(['unit'],axis=0).sum()




who = who_calculation(data_path,src_csv)

who = who[who['Age group'].str.contains('≥15 years',regex=True)]
who = who[who['Sex'].str.contains('Male|Female',regex=True)]
who = who[who['Measure'].str.contains('Deaths',regex=True)]
who=who.drop(columns=['Popultation-attributable fraction (%)','Rate (per 100,000 of population)'])
who = who[who['Year'] >1994]

country_code = list(who['Country'])

who.insert(4, 'ISO3', cc_all.convert(names = country_code, to='ISO3'))
who.insert(5, 'EXIO3', cc_all.convert(names = country_code, to='EXIO3'))
who = who[~who['EXIO3'].str.contains('not found',regex=True)]
who=who.drop(columns=['Risk Factor','Age group'])


who_aggregate=who.copy()
#who_aggregate=who_aggregate.groupby(['year','EXIO3','measure','estimate','sex'],axis=0).sum()
who_aggregate=who_aggregate.groupby(['Year','ISO3','Measure','Estimate','Sex'],axis=0).sum()



#injuries_split= pd.DataFrame(columns = ['ISO3','EXIOBASE sector','sex','estimate','year','death'])
#
#for code in injuries_fatal['geo\\time'].unique():
#    if code != 'not found':
#        print(code)
#        for a in injuries_fatal['nace_r2'].unique():
#            for b in who['sex'].unique():
#                for c in who['estimate'].unique():
#                    for year in range(2008,2020):
#                        injuries_split=injuries_split.append(pd.Series([code,a,b,c,year,0],index=[i for i in injuries_split.columns]),ignore_index=True)
#         
#for i in range(4,6):
#    injuries_split[injuries_split.columns[i]]=pd.to_numeric(injuries_split[injuries_split.columns[i]],errors = 'coerce')
#               
#for code in injuries_fatal['geo\\time'].unique():
#    if code in who['EXIO3'].unique():
#
#        for a in injuries_fatal['nace_r2'].unique():
#            for b in who['sex'].unique():
#                for c in who['estimate'].unique():
#                    for year in range(2008,2020):
#                        if (year==2010 or year==2016):
#                            print(year)
#                            print(code,a,b,c)
#                            injuries_split.loc[(injuries_split['ISO3']==code)&(injuries_split['EXIOBASE sector']==a)&(injuries_split['sex']==b)&(injuries_split['estimate']==c)&(injuries_split['year']==year),'death']=round(float(who_aggregate.loc[who_aggregate.index==(2010, code, 'Deaths', c,   b),'Number (DALYs in 1000s)'].to_string(index=False,header=False))*float(injuries_fatal.loc[(injuries_fatal['unit']=='NR')&(injuries_fatal['nace_r2']==a)&(injuries_fatal['geo\\time']==code),str(year)].to_string(index=False,header=False))/float(injuries_fatal_aggregate.loc[injuries_fatal_aggregate.index==('NR', code),str(year)].to_string(index=False,header=False)))
#    
#
#for code in injuries_fatal['geo\\time'].unique():
#    print(code)
#    if code in who['EXIO3'].unique():
#        for a in injuries_fatal['nace_r2'].unique():
#            if a == 'A':
#                for b in who['sex'].unique():
#                    for c in who['estimate'].unique():
#                        for year in range(2008,2020):
#                            if (year<=2009):
#                                print(year)
#                                print(code,a,b,c)
#                                if (float(injuries_fatal_aggregate.loc[injuries_fatal_aggregate.index==('NR', code),str(2010)].to_string(index=False,header=False)) !=0 and float(injuries_fatal_aggregate.loc[injuries_fatal_aggregate.index==('NR', code),str(year)].to_string(index=False,header=False))):
#                                    print(float(who_aggregate.loc[who_aggregate.index==(2010, code, 'Deaths', c,   b),'Number (DALYs in 1000s)'].to_string(index=False,header=False)))
#                                    unknown_who = round(float(who_aggregate.loc[who_aggregate.index==(2010, code, 'Deaths', c,   b),'Number (DALYs in 1000s)'].to_string(index=False,header=False)) + ((float(injuries_fatal_aggregate.loc[injuries_fatal_aggregate.index==('NR', code),str(year)].to_string(index=False,header=False))-float(injuries_fatal_aggregate.loc[injuries_fatal_aggregate.index==('NR', code),str(2010)].to_string(index=False,header=False)))/(float(injuries_fatal_aggregate.loc[injuries_fatal_aggregate.index==('NR', code),str(2010)].to_string(index=False,header=False)))))
#                                    injuries_split.loc[(injuries_split['ISO3']==code)&(injuries_split['EXIOBASE sector']==a)&(injuries_split['sex']==b)&(injuries_split['estimate']==c)&(injuries_split['year']==year),'death']=round(unknown_who*float(injuries_fatal.loc[(injuries_fatal['unit']=='NR')&(injuries_fatal['nace_r2']==a)&(injuries_fatal['geo\\time']==code),str(year)].to_string(index=False,header=False))/float(injuries_fatal_aggregate.loc[injuries_fatal_aggregate.index==('NR', code),str(year)].to_string(index=False,header=False)))
#                                    
#                                else:
#                                    injuries_split.loc[(injuries_split['ISO3']==code)&(injuries_split['EXIOBASE sector']==a)&(injuries_split['sex']==b)&(injuries_split['estimate']==c)&(injuries_split['year']==year),'death']=0
#                        for year in range(2008,2020):
#                            if (year>2010  & year<2016):
#                                print(year)
#                                print(code,a,b,c)
#                                if (float(injuries_fatal_aggregate.loc[injuries_fatal_aggregate.index==('NR', code),str(2016)].to_string(index=False,header=False)) !=0 and float(injuries_fatal_aggregate.loc[injuries_fatal_aggregate.index==('NR', code),str(year)].to_string(index=False,header=False))):
#                                    print(float(who_aggregate.loc[who_aggregate.index==(2016, code, 'Deaths', c,   b),'Number (DALYs in 1000s)'].to_string(index=False,header=False)))
#                                    unknown_who = round(float(who_aggregate.loc[who_aggregate.index==(2016, code, 'Deaths', c,   b),'Number (DALYs in 1000s)'].to_string(index=False,header=False)) + ((float(injuries_fatal_aggregate.loc[injuries_fatal_aggregate.index==('NR', code),str(year)].to_string(index=False,header=False))-float(injuries_fatal_aggregate.loc[injuries_fatal_aggregate.index==('NR', code),str(2016)].to_string(index=False,header=False)))/(float(injuries_fatal_aggregate.loc[injuries_fatal_aggregate.index==('NR', code),str(2016)].to_string(index=False,header=False)))))
#                                    injuries_split.loc[(injuries_split['ISO3']==code)&(injuries_split['EXIOBASE sector']==a)&(injuries_split['sex']==b)&(injuries_split['estimate']==c)&(injuries_split['year']==year),'death']=round(unknown_who*float(injuries_fatal.loc[(injuries_fatal['unit']=='NR')&(injuries_fatal['nace_r2']==a)&(injuries_fatal['geo\\time']==code),str(year)].to_string(index=False,header=False))/float(injuries_fatal_aggregate.loc[injuries_fatal_aggregate.index==('NR', code),str(year)].to_string(index=False,header=False)))
#                                    
#                                else:
#                                    injuries_split.loc[(injuries_split['ISO3']==code)&(injuries_split['EXIOBASE sector']==a)&(injuries_split['sex']==b)&(injuries_split['estimate']==c)&(injuries_split['year']==year),'death']=0
#                        for year in range(2008,2020):
#                            if (year>2016):
#                                print(year)
#                                print(code,a,b,c)
#                                if (float(injuries_fatal_aggregate.loc[injuries_fatal_aggregate.index==('NR', code),str(2016)].to_string(index=False,header=False)) !=0 and float(injuries_fatal_aggregate.loc[injuries_fatal_aggregate.index==('NR', code),str(year)].to_string(index=False,header=False))):
#                                    print(float(who_aggregate.loc[who_aggregate.index==(2016, code, 'Deaths', c,   b),'Number (DALYs in 1000s)'].to_string(index=False,header=False)))
#                                    unknown_who = round(float(who_aggregate.loc[who_aggregate.index==(2016, code, 'Deaths', c,   b),'Number (DALYs in 1000s)'].to_string(index=False,header=False)) + ((float(injuries_fatal_aggregate.loc[injuries_fatal_aggregate.index==('NR', code),str(year)].to_string(index=False,header=False))-float(injuries_fatal_aggregate.loc[injuries_fatal_aggregate.index==('NR', code),str(2016)].to_string(index=False,header=False)))/(float(injuries_fatal_aggregate.loc[injuries_fatal_aggregate.index==('NR', code),str(2016)].to_string(index=False,header=False)))))
#                                    injuries_split.loc[(injuries_split['ISO3']==code)&(injuries_split['EXIOBASE sector']==a)&(injuries_split['sex']==b)&(injuries_split['estimate']==c)&(injuries_split['year']==year),'death']=round(unknown_who*float(injuries_fatal.loc[(injuries_fatal['unit']=='NR')&(injuries_fatal['nace_r2']==a)&(injuries_fatal['geo\\time']==code),str(year)].to_string(index=False,header=False))/float(injuries_fatal_aggregate.loc[injuries_fatal_aggregate.index==('NR', code),str(year)].to_string(index=False,header=False)))
#                                    
#                                else:
#                                    injuries_split.loc[(injuries_split['ISO3']==code)&(injuries_split['EXIOBASE sector']==a)&(injuries_split['sex']==b)&(injuries_split['estimate']==c)&(injuries_split['year']==year),'death']=0
#

workforce = pd.read_csv("aux/table_workforce_by_ISO3.csv")
workforce = workforce[~workforce['classif1'].str.contains('SECTOR|AGGREGATE',regex=True)]
workforce = workforce[~workforce['sex'].str.contains('SEX_T',regex=True)]
workforce=workforce.replace(to_replace='SEX_F',value='Female')
workforce=workforce.replace(to_replace='SEX_M',value='Male')



injuries_split_ROW= pd.DataFrame(columns = ['ISO3','EXIOBASE sector','sex','estimate','year','death'])

for code in who['ISO3'].unique():
    if code != 'not found':
        print(code)
        for a in injuries_fatal['nace_r2'].unique():
            for b in who['Sex'].unique():
                for c in who['Estimate'].unique():
                    for year in range(2008,2020):
                        injuries_split_ROW=injuries_split_ROW.append(pd.Series([code,a,b,c,year,0],index=[i for i in injuries_split_ROW.columns]),ignore_index=True)

injuries_split_ROW_ini = injuries_split_ROW.copy()

for code in who['ISO3'].unique():
    print(code)
#    if  code  not in injuries_fatal['ISO3'].unique() :
#        for a in injuries_split_ROW['EXIOBASE sector'].unique():
#            for b in injuries_split_ROW['sex'].unique():
#                for c in who['estimate'].unique():
#                    for year in range(2008,2020):
#                        if (year==2010 or year==2016):
#                            if float(workforce.loc[(workforce.ref_area==code)&(workforce.time==year)&(workforce.sex==b)&(workforce.classif1=='ECO_DETAILS_TOTAL'),'obs_value'].to_string(index=False,header=False)) != 0.0:                                        
#                                if str('ECO_DETAILS_'+str(a)) in workforce.classif1.unique() : 
#                                    injuries_split_ROW.loc[(injuries_split_ROW['ISO3']==code)&(injuries_split_ROW['EXIOBASE sector']==a)&(injuries_split_ROW['sex']==b)&(injuries_split_ROW['estimate']==c)&(injuries_split_ROW['year']==year),'death']=round(float(who_aggregate.loc[who_aggregate.index==(year, code, 'Deaths', c, b),'Number (DALYs in 1000s)'].to_string(index=False,header=False))*float(workforce.loc[(workforce.ref_area==code)&(workforce.time==year)&(workforce.sex==b)&(workforce.classif1=='ECO_DETAILS_'+str(a)),'obs_value'].to_string(index=False,header=False))/float(workforce.loc[(workforce.ref_area==code)&(workforce.time==year)&(workforce.sex==b)&(workforce.classif1=='ECO_DETAILS_TOTAL'),'obs_value'].to_string(index=False,header=False)))
#                                if ((a == 'D') or (a == 'E')):
#                                    injuries_split_ROW.loc[(injuries_split_ROW['ISO3']==code)&(injuries_split_ROW['EXIOBASE sector']==a)&(injuries_split_ROW['sex']==b)&(injuries_split_ROW['estimate']==c)&(injuries_split_ROW['year']==year),'death']=round(float(who_aggregate.loc[who_aggregate.index==(year, code, 'Deaths', c, b),'Number (DALYs in 1000s)'].to_string(index=False,header=False))*0.5*float(workforce.loc[(workforce.ref_area==code)&(workforce.time==year)&(workforce.sex==b)&(workforce.classif1=='ECO_DETAILS_DE'),'obs_value'].to_string(index=False,header=False))/float(workforce.loc[(workforce.ref_area==code)&(workforce.time==year)&(workforce.sex==b)&(workforce.classif1=='ECO_DETAILS_TOTAL'),'obs_value'].to_string(index=False,header=False)))
#                                if ((a == 'H') or (a == 'J')):
#                                    injuries_split_ROW.loc[(injuries_split_ROW['ISO3']==code)&(injuries_split_ROW['EXIOBASE sector']==a)&(injuries_split_ROW['sex']==b)&(injuries_split_ROW['estimate']==c)&(injuries_split_ROW['year']==year),'death']=round(float(who_aggregate.loc[who_aggregate.index==(year, code, 'Deaths', c, b),'Number (DALYs in 1000s)'].to_string(index=False,header=False))*0.5*float(workforce.loc[(workforce.ref_area==code)&(workforce.time==year)&(workforce.sex==b)&(workforce.classif1=='ECO_DETAILS_HJ'),'obs_value'].to_string(index=False,header=False))/float(workforce.loc[(workforce.ref_area==code)&(workforce.time==year)&(workforce.sex==b)&(workforce.classif1=='ECO_DETAILS_TOTAL'),'obs_value'].to_string(index=False,header=False)))
#                                if ((a == 'L') or (a == 'M')or (a == 'N')):
#                                    injuries_split_ROW.loc[(injuries_split_ROW['ISO3']==code)&(injuries_split_ROW['EXIOBASE sector']==a)&(injuries_split_ROW['sex']==b)&(injuries_split_ROW['estimate']==c)&(injuries_split_ROW['year']==year),'death']=round(float(who_aggregate.loc[who_aggregate.index==(year, code, 'Deaths', c, b),'Number (DALYs in 1000s)'].to_string(index=False,header=False))*(1/3)*float(workforce.loc[(workforce.ref_area==code)&(workforce.time==year)&(workforce.sex==b)&(workforce.classif1=='ECO_DETAILS_LMN'),'obs_value'].to_string(index=False,header=False))/float(workforce.loc[(workforce.ref_area==code)&(workforce.time==year)&(workforce.sex==b)&(workforce.classif1=='ECO_DETAILS_TOTAL'),'obs_value'].to_string(index=False,header=False)))
#                                if ((a == 'R') or (a == 'S')or (a == 'T')or (a == 'U')):
#                                    injuries_split_ROW.loc[(injuries_split_ROW['ISO3']==code)&(injuries_split_ROW['EXIOBASE sector']==a)&(injuries_split_ROW['sex']==b)&(injuries_split_ROW['estimate']==c)&(injuries_split_ROW['year']==year),'death']=round(float(who_aggregate.loc[who_aggregate.index==(year, code, 'Deaths', c, b),'Number (DALYs in 1000s)'].to_string(index=False,header=False))*(1/4)*float(workforce.loc[(workforce.ref_area==code)&(workforce.time==year)&(workforce.sex==b)&(workforce.classif1=='ECO_DETAILS_RSTU'),'obs_value'].to_string(index=False,header=False))/float(workforce.loc[(workforce.ref_area==code)&(workforce.time==year)&(workforce.sex==b)&(workforce.classif1=='ECO_DETAILS_TOTAL'),'obs_value'].to_string(index=False,header=False)))
#                            else :
#                                injuries_split_ROW.loc[(injuries_split_ROW['ISO3']==code)&(injuries_split_ROW['EXIOBASE sector']==a)&(injuries_split_ROW['sex']==b)&(injuries_split_ROW['estimate']==c)&(injuries_split_ROW['year']==year),'death']=0

    '''this upper part needed to be modfied:
        '''

    if  code  not in injuries_fatal['ISO3'].unique() :
        for a in injuries_split_ROW['EXIOBASE sector'].unique():
            for b in injuries_split_ROW['sex'].unique():
                for c in who['Estimate'].unique():
                    for year in range(2008,2020):
                        if (year==2010 or year==2016):
                            if float(injuries_fatal_aggregate_nace[str(year)]['NR', str(a)]) != 0.0:                                        
                                injuries_split_ROW.loc[(injuries_split_ROW['ISO3']==code)&(injuries_split_ROW['EXIOBASE sector']==a)&(injuries_split_ROW['sex']==b)&(injuries_split_ROW['estimate']==c)&(injuries_split_ROW['year']==year),'death']=(float(who_aggregate.loc[who_aggregate.index==(year, code, 'Deaths', c, b),'Number (for DALYs in 1000s)'].to_string(index=False,header=False))*float(injuries_fatal_aggregate_nace[str(year)]['NR', str(a)])/float(injuries_fatal_aggregate_year[str(year)]['NR']))
                            else :
                                injuries_split_ROW.loc[(injuries_split_ROW['ISO3']==code)&(injuries_split_ROW['EXIOBASE sector']==a)&(injuries_split_ROW['sex']==b)&(injuries_split_ROW['estimate']==c)&(injuries_split_ROW['year']==year),'death']=0



                        
    else:
        for a in injuries_split_ROW['EXIOBASE sector'].unique():
            for b in injuries_split_ROW['sex'].unique():
                for c in who['Estimate'].unique():
                    for year in range(2008,2020):
                        if (year==2010 or year==2016):
                            if float(injuries_fatal_aggregate.loc[injuries_fatal_aggregate.index==('NR', code),str(year)].to_string(index=False,header=False)) != 0.0:
                                injuries_split_ROW.loc[(injuries_split_ROW['ISO3']==code)&(injuries_split_ROW['EXIOBASE sector']==a)&(injuries_split_ROW['sex']==b)&(injuries_split_ROW['estimate']==c)&(injuries_split_ROW['year']==year),'death']=(float(who_aggregate.loc[who_aggregate.index==(year, code, 'Deaths', c,   b),'Number (for DALYs in 1000s)'].to_string(index=False,header=False))*float(injuries_fatal.loc[(injuries_fatal['unit']=='NR')&(injuries_fatal['nace_r2']==a)&(injuries_fatal['ISO3']==code),str(year)].to_string(index=False,header=False))/float(injuries_fatal_aggregate.loc[injuries_fatal_aggregate.index==('NR', code),str(year)].to_string(index=False,header=False)))
                            else :
                                injuries_split_ROW.loc[(injuries_split_ROW['ISO3']==code)&(injuries_split_ROW['EXIOBASE sector']==a)&(injuries_split_ROW['sex']==b)&(injuries_split_ROW['estimate']==c)&(injuries_split_ROW['year']==year),'death']=0
                            

correspondance = pd.read_csv("aux/countries_en.csv")
ratio = pd.read_csv("aux/ratio.csv")
Sub_region_name = []
for a in ratio['Sub-region Name']:
    if not pd.isnull(a):
        Sub_region_name.append(a)
region_name = []
for a in ratio['Region name']:
    if not pd.isnull(a):
        region_name.append(a)
#injuries_split_ROW=read_csv        


#for code in who['ISO3'].unique(): #14:06 29.04
for code in who['ISO3'].unique():
    print(code)
    if  code  in injuries_fatal['ISO3'].unique():
        for a in injuries_split_ROW['EXIOBASE sector'].unique():
            for b in injuries_split_ROW['sex'].unique():
                for c in who['Estimate'].unique():
                    for year in range(2009,2020):
                        if (year<2010):
                            if (float(injuries_fatal_aggregate.loc[injuries_fatal_aggregate.index==('NR', code),str(2010)].to_string(index=False,header=False)) !=0 and float(injuries_fatal_aggregate.loc[injuries_fatal_aggregate.index==('NR', code),str(year)].to_string(index=False,header=False)) != 0.0):
                                unknown_who = (float(who_aggregate.loc[who_aggregate.index==(2010, code, 'Deaths', c,   b),'Number (for DALYs in 1000s)'].to_string(index=False,header=False)) + ((float(injuries_fatal_aggregate.loc[injuries_fatal_aggregate.index==('NR', code),str(year)].to_string(index=False,header=False))-float(injuries_fatal_aggregate.loc[injuries_fatal_aggregate.index==('NR', code),str(2010)].to_string(index=False,header=False)))/(float(injuries_fatal_aggregate.loc[injuries_fatal_aggregate.index==('NR', code),str(2010)].to_string(index=False,header=False)))))
                                injuries_split_ROW.loc[(injuries_split_ROW['ISO3']==code)&(injuries_split_ROW['EXIOBASE sector']==a)&(injuries_split_ROW['sex']==b)&(injuries_split_ROW['estimate']==c)&(injuries_split_ROW['year']==year),'death']=(unknown_who*float(injuries_fatal.loc[(injuries_fatal['unit']=='NR')&(injuries_fatal['nace_r2']==a)&(injuries_fatal['ISO3']==code),str(year)].to_string(index=False,header=False))/float(injuries_fatal_aggregate.loc[injuries_fatal_aggregate.index==('NR', code),str(year)].to_string(index=False,header=False)))
                                
                            else:
                                injuries_split_ROW.loc[(injuries_split_ROW['ISO3']==code)&(injuries_split_ROW['EXIOBASE sector']==a)&(injuries_split_ROW['sex']==b)&(injuries_split_ROW['estimate']==c)&(injuries_split_ROW['year']==year),'death']=0
                    

                        if (year>2010  & year<2016):
                            if (float(injuries_fatal_aggregate.loc[injuries_fatal_aggregate.index==('NR', code),str(2016)].to_string(index=False,header=False)) !=0 and float(injuries_fatal_aggregate.loc[injuries_fatal_aggregate.index==('NR', code),str(year)].to_string(index=False,header=False)) != 0.0):
                                unknown_who = (float(who_aggregate.loc[who_aggregate.index==(2016, code, 'Deaths', c,   b),'Number (for DALYs in 1000s)'].to_string(index=False,header=False)) + ((float(injuries_fatal_aggregate.loc[injuries_fatal_aggregate.index==('NR', code),str(year)].to_string(index=False,header=False))-float(injuries_fatal_aggregate.loc[injuries_fatal_aggregate.index==('NR', code),str(2016)].to_string(index=False,header=False)))/(float(injuries_fatal_aggregate.loc[injuries_fatal_aggregate.index==('NR', code),str(2016)].to_string(index=False,header=False)))))
                                injuries_split_ROW.loc[(injuries_split_ROW['ISO3']==code)&(injuries_split_ROW['EXIOBASE sector']==a)&(injuries_split_ROW['sex']==b)&(injuries_split_ROW['estimate']==c)&(injuries_split_ROW['year']==year),'death']=(unknown_who*float(injuries_fatal.loc[(injuries_fatal['unit']=='NR')&(injuries_fatal['nace_r2']==a)&(injuries_fatal['ISO3']==code),str(year)].to_string(index=False,header=False))/float(injuries_fatal_aggregate.loc[injuries_fatal_aggregate.index==('NR', code),str(year)].to_string(index=False,header=False)))
                                
                            else:
                                injuries_split_ROW.loc[(injuries_split_ROW['ISO3']==code)&(injuries_split_ROW['EXIOBASE sector']==a)&(injuries_split_ROW['sex']==b)&(injuries_split_ROW['estimate']==c)&(injuries_split_ROW['year']==year),'death']=0

                        if (year>2016):
                            if (float(injuries_fatal_aggregate.loc[injuries_fatal_aggregate.index==('NR', code),str(2016)].to_string(index=False,header=False)) !=0 and float(injuries_fatal_aggregate.loc[injuries_fatal_aggregate.index==('NR', code),str(year)].to_string(index=False,header=False)) != 0.0):
                                unknown_who = (float(who_aggregate.loc[who_aggregate.index==(2016, code, 'Deaths', c,   b),'Number (for DALYs in 1000s)'].to_string(index=False,header=False)) + ((float(injuries_fatal_aggregate.loc[injuries_fatal_aggregate.index==('NR', code),str(year)].to_string(index=False,header=False))-float(injuries_fatal_aggregate.loc[injuries_fatal_aggregate.index==('NR', code),str(2016)].to_string(index=False,header=False)))/(float(injuries_fatal_aggregate.loc[injuries_fatal_aggregate.index==('NR', code),str(2016)].to_string(index=False,header=False)))))
                                injuries_split_ROW.loc[(injuries_split_ROW['ISO3']==code)&(injuries_split_ROW['EXIOBASE sector']==a)&(injuries_split_ROW['sex']==b)&(injuries_split_ROW['estimate']==c)&(injuries_split_ROW['year']==year),'death']=(unknown_who*float(injuries_fatal.loc[(injuries_fatal['unit']=='NR')&(injuries_fatal['nace_r2']==a)&(injuries_fatal['ISO3']==code),str(year)].to_string(index=False,header=False))/float(injuries_fatal_aggregate.loc[injuries_fatal_aggregate.index==('NR', code),str(year)].to_string(index=False,header=False)))
                                
                            else:
                                injuries_split_ROW.loc[(injuries_split_ROW['ISO3']==code)&(injuries_split_ROW['EXIOBASE sector']==a)&(injuries_split_ROW['sex']==b)&(injuries_split_ROW['estimate']==c)&(injuries_split_ROW['year']==year),'death']=0

   
    
    if  code  not in injuries_fatal['ISO3'].unique():
        region_broad = correspondance.loc[(correspondance['ISO3 Code']==code),'ILO Subregion - Broad'].to_string(index=False,header=False)
        region = correspondance.loc[(correspondance['ISO3 Code']==code),'ILO Region'].to_string(index=False,header=False)
        for a in injuries_split_ROW['EXIOBASE sector'].unique():
            for b in injuries_split_ROW['sex'].unique():
                for c in who['Estimate'].unique():
                    for year in range(2009,2020):                          
                    
                        if region_broad  in Sub_region_name :
                            if c=='Point':
                                #print('1',code)
                                if str('ECO_DETAILS_'+str(a)) in workforce.classif1.unique() : 
                                    pop_year = float(workforce.loc[(workforce.ref_area==code)&(workforce.time==year)&(workforce.sex==b)&(workforce.classif1=='ECO_DETAILS_'+str(a)),'obs_value'].to_string(index=False,header=False))
                                    ratio_region_ISIC = float(ratio.loc[(ratio['Sub-region Name']==region_broad),'ECO_ISIC4_'+str(a)].to_string(index=False,header=False))
                                    death_year = (float(pop_year * ratio_region_ISIC))
                                    injuries_split_ROW.loc[(injuries_split_ROW['ISO3']==code)&(injuries_split_ROW['EXIOBASE sector']==a)&(injuries_split_ROW['sex']==b)&(injuries_split_ROW['estimate']==c)&(injuries_split_ROW['year']==year),'death']=death_year
                                
                                if ((a == 'D') or (a == 'E')):
                                    pop_year = float(workforce.loc[(workforce.ref_area==code)&(workforce.time==year)&(workforce.sex==b)&(workforce.classif1=='ECO_DETAILS_DE'),'obs_value'].to_string(index=False,header=False))
                                    pop_year = 0.5 * pop_year       
                                    ratio_region_ISIC = float(ratio.loc[(ratio['Sub-region Name']==region_broad),'ECO_ISIC4_'+str(a)].to_string(index=False,header=False))
                                    death_year = (float(pop_year * ratio_region_ISIC))
                                    injuries_split_ROW.loc[(injuries_split_ROW['ISO3']==code)&(injuries_split_ROW['EXIOBASE sector']==a)&(injuries_split_ROW['sex']==b)&(injuries_split_ROW['estimate']==c)&(injuries_split_ROW['year']==year),'death']=death_year
                                
                                if ((a == 'H') or (a == 'J')):
                                    pop_year = float(workforce.loc[(workforce.ref_area==code)&(workforce.time==year)&(workforce.sex==b)&(workforce.classif1=='ECO_DETAILS_HJ'),'obs_value'].to_string(index=False,header=False))
                                    pop_year = 0.5 * pop_year       
                                    ratio_region_ISIC = float(ratio.loc[(ratio['Sub-region Name']==region_broad),'ECO_ISIC4_'+str(a)].to_string(index=False,header=False))
                                    death_year = (float(pop_year * ratio_region_ISIC))
                                    injuries_split_ROW.loc[(injuries_split_ROW['ISO3']==code)&(injuries_split_ROW['EXIOBASE sector']==a)&(injuries_split_ROW['sex']==b)&(injuries_split_ROW['estimate']==c)&(injuries_split_ROW['year']==year),'death']=death_year
                                                                                            
                                if ((a == 'L') or (a == 'M')or (a == 'N')):
                                    pop_year = float(workforce.loc[(workforce.ref_area==code)&(workforce.time==year)&(workforce.sex==b)&(workforce.classif1=='ECO_DETAILS_LMN'),'obs_value'].to_string(index=False,header=False))
                                    pop_year = (1/3) * pop_year                                
                                    ratio_region_ISIC = float(ratio.loc[(ratio['Sub-region Name']==region_broad),'ECO_ISIC4_'+str(a)].to_string(index=False,header=False))
                                    death_year = (float(pop_year * ratio_region_ISIC))
                                    injuries_split_ROW.loc[(injuries_split_ROW['ISO3']==code)&(injuries_split_ROW['EXIOBASE sector']==a)&(injuries_split_ROW['sex']==b)&(injuries_split_ROW['estimate']==c)&(injuries_split_ROW['year']==year),'death']=death_year
                                                                      
                                if ((a == 'R') or (a == 'S')or (a == 'T') or (a == 'U')):
                                    pop_year = float(workforce.loc[(workforce.ref_area==code)&(workforce.time==year)&(workforce.sex==b)&(workforce.classif1=='ECO_DETAILS_RSTU'),'obs_value'].to_string(index=False,header=False))
                                    pop_year = (1/4) * pop_year    
                                    ratio_region_ISIC = float(ratio.loc[(ratio['Sub-region Name']==region_broad),'ECO_ISIC4_'+str(a)].to_string(index=False,header=False))
                                    death_year = (float(pop_year * ratio_region_ISIC))
                                    injuries_split_ROW.loc[(injuries_split_ROW['ISO3']==code)&(injuries_split_ROW['EXIOBASE sector']==a)&(injuries_split_ROW['sex']==b)&(injuries_split_ROW['estimate']==c)&(injuries_split_ROW['year']==year),'death']=death_year
                                     
#                        if region == 'Asia and the Pacific':
#                            print(code,year)
                        elif region in region_name:
                            if c=='Point':
                                #print('2',code)

                                if str('ECO_DETAILS_'+str(a)) in workforce.classif1.unique() : 
                                    pop_year = float(workforce.loc[(workforce.ref_area==code)&(workforce.time==year)&(workforce.sex==b)&(workforce.classif1=='ECO_DETAILS_'+str(a)),'obs_value'].to_string(index=False,header=False))
                                    ratio_region_ISIC = float(ratio.loc[(ratio['Region name']==region),'ECO_ISIC4_'+str(a)].to_string(index=False,header=False))
                                    death_year = (float(pop_year * ratio_region_ISIC))
                                    injuries_split_ROW.loc[(injuries_split_ROW['ISO3']==code)&(injuries_split_ROW['EXIOBASE sector']==a)&(injuries_split_ROW['sex']==b)&(injuries_split_ROW['estimate']==c)&(injuries_split_ROW['year']==year),'death']=death_year
    
                                if ((a == 'D') or (a == 'E')):
                                    pop_year = float(workforce.loc[(workforce.ref_area==code)&(workforce.time==year)&(workforce.sex==b)&(workforce.classif1=='ECO_DETAILS_DE'),'obs_value'].to_string(index=False,header=False))
                                    pop_year = 0.5 * pop_year             
                                    ratio_region_ISIC = float(ratio.loc[(ratio['Region name']==region),'ECO_ISIC4_'+str(a)].to_string(index=False,header=False))
                                    death_year = (float(pop_year * ratio_region_ISIC))
                                    injuries_split_ROW.loc[(injuries_split_ROW['ISO3']==code)&(injuries_split_ROW['EXIOBASE sector']==a)&(injuries_split_ROW['sex']==b)&(injuries_split_ROW['estimate']==c)&(injuries_split_ROW['year']==year),'death']=death_year
    
                                if ((a == 'H') or (a == 'J')):
                                    pop_year = float(workforce.loc[(workforce.ref_area==code)&(workforce.time==year)&(workforce.sex==b)&(workforce.classif1=='ECO_DETAILS_HJ'),'obs_value'].to_string(index=False,header=False))
                                    pop_year = 0.5 * pop_year  
                                    ratio_region_ISIC = float(ratio.loc[(ratio['Region name']==region),'ECO_ISIC4_'+str(a)].to_string(index=False,header=False))
                                    death_year = (float(pop_year * ratio_region_ISIC))
                                    injuries_split_ROW.loc[(injuries_split_ROW['ISO3']==code)&(injuries_split_ROW['EXIOBASE sector']==a)&(injuries_split_ROW['sex']==b)&(injuries_split_ROW['estimate']==c)&(injuries_split_ROW['year']==year),'death']=death_year
    
    
                                if ((a == 'L') or (a == 'M')or (a == 'N')):
                                    pop_year = float(workforce.loc[(workforce.ref_area==code)&(workforce.time==year)&(workforce.sex==b)&(workforce.classif1=='ECO_DETAILS_LMN'),'obs_value'].to_string(index=False,header=False))
                                    pop_year = (1/3) * pop_year 
                                    ratio_region_ISIC = float(ratio.loc[(ratio['Region name']==region),'ECO_ISIC4_'+str(a)].to_string(index=False,header=False))
                                    death_year = (float(pop_year * ratio_region_ISIC))
                                    injuries_split_ROW.loc[(injuries_split_ROW['ISO3']==code)&(injuries_split_ROW['EXIOBASE sector']==a)&(injuries_split_ROW['sex']==b)&(injuries_split_ROW['estimate']==c)&(injuries_split_ROW['year']==year),'death']=death_year
    
                                if ((a == 'R') or (a == 'S')or (a == 'T') or (a == 'U')):
                                    pop_year = float(workforce.loc[(workforce.ref_area==code)&(workforce.time==year)&(workforce.sex==b)&(workforce.classif1=='ECO_DETAILS_RSTU'),'obs_value'].to_string(index=False,header=False))
                                    pop_year = (1/4) * pop_year 
                                    ratio_region_ISIC = float(ratio.loc[(ratio['Region name']==region),'ECO_ISIC4_'+str(a)].to_string(index=False,header=False))
                                    death_year = (float(pop_year * ratio_region_ISIC))
                                    injuries_split_ROW.loc[(injuries_split_ROW['ISO3']==code)&(injuries_split_ROW['EXIOBASE sector']==a)&(injuries_split_ROW['sex']==b)&(injuries_split_ROW['estimate']==c)&(injuries_split_ROW['year']==year),'death']=death_year
                            
                        else:
                            #print('3',code)

                            if float(workforce.loc[(workforce.ref_area==code)&(workforce.time==year)&(workforce.sex==b)&(workforce.classif1=='ECO_DETAILS_TOTAL'),'obs_value'].to_string(index=False,header=False)) != 0.0:
                                       
                                if str('ECO_DETAILS_'+str(a)) in workforce.classif1.unique() : 
                                    death2010=injuries_split_ROW.loc[(injuries_split_ROW['ISO3']==code)&(injuries_split_ROW['EXIOBASE sector']==a)&(injuries_split_ROW['sex']==b)&(injuries_split_ROW['estimate']==c)&(injuries_split_ROW['year']==2010),'death']
                                    death2016=injuries_split_ROW.loc[(injuries_split_ROW['ISO3']==code)&(injuries_split_ROW['EXIOBASE sector']==a)&(injuries_split_ROW['sex']==b)&(injuries_split_ROW['estimate']==c)&(injuries_split_ROW['year']==2016),'death']
                                    pop2010=float(workforce.loc[(workforce.ref_area==code)&(workforce.time==2010)&(workforce.sex==b)&(workforce.classif1=='ECO_DETAILS_'+str(a)),'obs_value'].to_string(index=False,header=False))
                                    pop2016=float(workforce.loc[(workforce.ref_area==code)&(workforce.time==2016)&(workforce.sex==b)&(workforce.classif1=='ECO_DETAILS_'+str(a)),'obs_value'].to_string(index=False,header=False))
    
                                    ratio2010=float(100*(death2010/pop2010))
                                    ratio2016=float(100*(death2016/pop2016))
                                    delta_ratio=float((ratio2016-ratio2010)/6.0)
                                    pop_year = float(workforce.loc[(workforce.ref_area==code)&(workforce.time==year)&(workforce.sex==b)&(workforce.classif1=='ECO_DETAILS_'+str(a)),'obs_value'].to_string(index=False,header=False))
                                    
                                    ratio_year = float((year-2010)*delta_ratio+ratio2010)
                                    death_year = (float(pop_year * ratio_year/100))
                                    if death_year > 0.0:
                                        injuries_split_ROW.loc[(injuries_split_ROW['ISO3']==code)&(injuries_split_ROW['EXIOBASE sector']==a)&(injuries_split_ROW['sex']==b)&(injuries_split_ROW['estimate']==c)&(injuries_split_ROW['year']==year),'death']=death_year
                                    else :
                                        injuries_split_ROW.loc[(injuries_split_ROW['ISO3']==code)&(injuries_split_ROW['EXIOBASE sector']==a)&(injuries_split_ROW['sex']==b)&(injuries_split_ROW['estimate']==c)&(injuries_split_ROW['year']==year),'death']=0
    
    
                                if ((a == 'D') or (a == 'E')):
                                    
                                    death2010=float(injuries_split_ROW.loc[(injuries_split_ROW['ISO3']==code)&(injuries_split_ROW['EXIOBASE sector']==a)&(injuries_split_ROW['sex']==b)&(injuries_split_ROW['estimate']==c)&(injuries_split_ROW['year']==2010),'death'].to_string(index=False,header=False))
                                    death2016=float(injuries_split_ROW.loc[(injuries_split_ROW['ISO3']==code)&(injuries_split_ROW['EXIOBASE sector']==a)&(injuries_split_ROW['sex']==b)&(injuries_split_ROW['estimate']==c)&(injuries_split_ROW['year']==2016),'death'].to_string(index=False,header=False))
                                    
                                    pop2010=float(workforce.loc[(workforce.ref_area==code)&(workforce.time==2010)&(workforce.sex==b)&(workforce.classif1=='ECO_DETAILS_DE'),'obs_value'].to_string(index=False,header=False))
                                    pop2010 = 0.5* pop2010
                                    pop2016=float(workforce.loc[(workforce.ref_area==code)&(workforce.time==2016)&(workforce.sex==b)&(workforce.classif1=='ECO_DETAILS_DE'),'obs_value'].to_string(index=False,header=False))
                                    pop2016 = 0.5*pop2016
                                    ratio2010=float(100*(death2010/pop2010))
                                    ratio2016=float(100*(death2016/pop2016))
                                    delta_ratio=float((ratio2016-ratio2010)/6.0)
                                    pop_year = float(workforce.loc[(workforce.ref_area==code)&(workforce.time==year)&(workforce.sex==b)&(workforce.classif1=='ECO_DETAILS_DE'),'obs_value'].to_string(index=False,header=False))
                                    pop_year = 0.5 * pop_year
                                    ratio_year = float((year-2010)*delta_ratio+ratio2010)
                                    death_year = (float(pop_year * ratio_year/100))
                                    if death_year > 0.0:
                                        injuries_split_ROW.loc[(injuries_split_ROW['ISO3']==code)&(injuries_split_ROW['EXIOBASE sector']==a)&(injuries_split_ROW['sex']==b)&(injuries_split_ROW['estimate']==c)&(injuries_split_ROW['year']==year),'death']=death_year
                                    else :
                                        injuries_split_ROW.loc[(injuries_split_ROW['ISO3']==code)&(injuries_split_ROW['EXIOBASE sector']==a)&(injuries_split_ROW['sex']==b)&(injuries_split_ROW['estimate']==c)&(injuries_split_ROW['year']==year),'death']=0
    
    
                                if ((a == 'H') or (a == 'J')):
                                    
                                    death2010=float(injuries_split_ROW.loc[(injuries_split_ROW['ISO3']==code)&(injuries_split_ROW['EXIOBASE sector']==a)&(injuries_split_ROW['sex']==b)&(injuries_split_ROW['estimate']==c)&(injuries_split_ROW['year']==2010),'death'].to_string(index=False,header=False))
                                    death2016=float(injuries_split_ROW.loc[(injuries_split_ROW['ISO3']==code)&(injuries_split_ROW['EXIOBASE sector']==a)&(injuries_split_ROW['sex']==b)&(injuries_split_ROW['estimate']==c)&(injuries_split_ROW['year']==2016),'death'].to_string(index=False,header=False))
                                    
                                    pop2010=float(workforce.loc[(workforce.ref_area==code)&(workforce.time==2010)&(workforce.sex==b)&(workforce.classif1=='ECO_DETAILS_HJ'),'obs_value'].to_string(index=False,header=False))
                                    pop2010 = 0.5* pop2010
                                    pop2016=float(workforce.loc[(workforce.ref_area==code)&(workforce.time==2016)&(workforce.sex==b)&(workforce.classif1=='ECO_DETAILS_HJ'),'obs_value'].to_string(index=False,header=False))
                                    pop2016 = 0.5*pop2016
                                    ratio2010=float(100*(death2010/pop2010))
                                    ratio2016=float(100*(death2016/pop2016))
                                    delta_ratio=float((ratio2016-ratio2010)/6.0)
                                    pop_year = float(workforce.loc[(workforce.ref_area==code)&(workforce.time==year)&(workforce.sex==b)&(workforce.classif1=='ECO_DETAILS_HJ'),'obs_value'].to_string(index=False,header=False))
                                    pop_year = 0.5 * pop_year
                                    ratio_year = float((year-2010)*delta_ratio+ratio2010)
                                    death_year = (float(pop_year * ratio_year/100))
                                    if death_year > 0.0:
                                        injuries_split_ROW.loc[(injuries_split_ROW['ISO3']==code)&(injuries_split_ROW['EXIOBASE sector']==a)&(injuries_split_ROW['sex']==b)&(injuries_split_ROW['estimate']==c)&(injuries_split_ROW['year']==year),'death']=death_year
                                    else :
                                        injuries_split_ROW.loc[(injuries_split_ROW['ISO3']==code)&(injuries_split_ROW['EXIOBASE sector']==a)&(injuries_split_ROW['sex']==b)&(injuries_split_ROW['estimate']==c)&(injuries_split_ROW['year']==year),'death']=0
    
    
    
    
                                if ((a == 'L') or (a == 'M')or (a == 'N')):
                                    
                                    death2010=float(injuries_split_ROW.loc[(injuries_split_ROW['ISO3']==code)&(injuries_split_ROW['EXIOBASE sector']==a)&(injuries_split_ROW['sex']==b)&(injuries_split_ROW['estimate']==c)&(injuries_split_ROW['year']==2010),'death'].to_string(index=False,header=False))
                                    death2016=float(injuries_split_ROW.loc[(injuries_split_ROW['ISO3']==code)&(injuries_split_ROW['EXIOBASE sector']==a)&(injuries_split_ROW['sex']==b)&(injuries_split_ROW['estimate']==c)&(injuries_split_ROW['year']==2016),'death'].to_string(index=False,header=False))
                                    
                                    pop2010=float(workforce.loc[(workforce.ref_area==code)&(workforce.time==2010)&(workforce.sex==b)&(workforce.classif1=='ECO_DETAILS_LMN'),'obs_value'].to_string(index=False,header=False))
                                    pop2010 = (1/3)* pop2010
                                    pop2016=float(workforce.loc[(workforce.ref_area==code)&(workforce.time==2016)&(workforce.sex==b)&(workforce.classif1=='ECO_DETAILS_LMN'),'obs_value'].to_string(index=False,header=False))
                                    pop2016 = (1/3)*pop2016
                                    ratio2010=float(100*(death2010/pop2010))
                                    ratio2016=float(100*(death2016/pop2016))
                                    delta_ratio=float((ratio2016-ratio2010)/6.0)
                                    pop_year = float(workforce.loc[(workforce.ref_area==code)&(workforce.time==year)&(workforce.sex==b)&(workforce.classif1=='ECO_DETAILS_LMN'),'obs_value'].to_string(index=False,header=False))
                                    pop_year = (1/3) * pop_year
                                    ratio_year = float((year-2010)*delta_ratio+ratio2010)
                                    death_year = (float(pop_year * ratio_year/100))
                                    if death_year > 0.0:
                                        injuries_split_ROW.loc[(injuries_split_ROW['ISO3']==code)&(injuries_split_ROW['EXIOBASE sector']==a)&(injuries_split_ROW['sex']==b)&(injuries_split_ROW['estimate']==c)&(injuries_split_ROW['year']==year),'death']=death_year
                                    else :
                                        injuries_split_ROW.loc[(injuries_split_ROW['ISO3']==code)&(injuries_split_ROW['EXIOBASE sector']==a)&(injuries_split_ROW['sex']==b)&(injuries_split_ROW['estimate']==c)&(injuries_split_ROW['year']==year),'death']=0
    
                                    
                                    
                                    
                                if ((a == 'R') or (a == 'S')or (a == 'T') or (a == 'U')):
                                    
                                    death2010=float(injuries_split_ROW.loc[(injuries_split_ROW['ISO3']==code)&(injuries_split_ROW['EXIOBASE sector']==a)&(injuries_split_ROW['sex']==b)&(injuries_split_ROW['estimate']==c)&(injuries_split_ROW['year']==2010),'death'].to_string(index=False,header=False))
                                    death2016=float(injuries_split_ROW.loc[(injuries_split_ROW['ISO3']==code)&(injuries_split_ROW['EXIOBASE sector']==a)&(injuries_split_ROW['sex']==b)&(injuries_split_ROW['estimate']==c)&(injuries_split_ROW['year']==2016),'death'].to_string(index=False,header=False))
                                    
                                    pop2010=float(workforce.loc[(workforce.ref_area==code)&(workforce.time==2010)&(workforce.sex==b)&(workforce.classif1=='ECO_DETAILS_RSTU'),'obs_value'].to_string(index=False,header=False))
                                    pop2010 = (1/4)* pop2010
                                    pop2016=float(workforce.loc[(workforce.ref_area==code)&(workforce.time==2016)&(workforce.sex==b)&(workforce.classif1=='ECO_DETAILS_RSTU'),'obs_value'].to_string(index=False,header=False))
                                    pop2016 = (1/4)*pop2016
                                    ratio2010=float(100*(death2010/pop2010))
                                    ratio2016=float(100*(death2016/pop2016))
                                    delta_ratio=float((ratio2016-ratio2010)/6.0)
                                    pop_year = float(workforce.loc[(workforce.ref_area==code)&(workforce.time==year)&(workforce.sex==b)&(workforce.classif1=='ECO_DETAILS_RSTU'),'obs_value'].to_string(index=False,header=False))
                                    pop_year = (1/4) * pop_year
                                    ratio_year = float((year-2010)*delta_ratio+ratio2010)
                                    death_year = (float(pop_year * ratio_year/100))
                                    if death_year > 0.0:
                                        injuries_split_ROW.loc[(injuries_split_ROW['ISO3']==code)&(injuries_split_ROW['EXIOBASE sector']==a)&(injuries_split_ROW['sex']==b)&(injuries_split_ROW['estimate']==c)&(injuries_split_ROW['year']==year),'death']=death_year
                                    else :
                                        injuries_split_ROW.loc[(injuries_split_ROW['ISO3']==code)&(injuries_split_ROW['EXIOBASE sector']==a)&(injuries_split_ROW['sex']==b)&(injuries_split_ROW['estimate']==c)&(injuries_split_ROW['year']==year),'death']=0

                             
                    

                            else :
                                injuries_split_ROW.loc[(injuries_split_ROW['ISO3']==code)&(injuries_split_ROW['EXIOBASE sector']==a)&(injuries_split_ROW['sex']==b)&(injuries_split_ROW['estimate']==c)&(injuries_split_ROW['year']==year),'death']=0


    injuries_split_ROW.to_csv("death_per_ISO3.csv",index=False)
injuries_split_ROW.to_csv("death_per_ISO3.csv",index=False)

#injuries_split_ROW_drop=injuries_split_ROW.copy()
#
#for row in injuries_split_ROW_drop.index:
#    code = injuries_split_ROW_drop.iloc[row]['ISO3']
#    print(code)  
#    if  code  not in injuries_fatal['ISO3'].unique():
#        region_broad = correspondance.loc[(correspondance['ISO3 Code']==code),'ILO Subregion - Broad'].to_string(index=False,header=False)
#        region = correspondance.loc[(correspondance['ISO3 Code']==code),'ILO Region'].to_string(index=False,header=False)
#        if region_broad  in Sub_region_name :
#            injuries_split_ROW_drop.loc[injuries_split_ROW_drop['ISO3']==code] = injuries_split_ROW_drop.loc[injuries_split_ROW_drop['ISO3']==code][injuries_split_ROW_drop["estimate"].str.contains("Upper") == False]
#
#    
#for code in who['ISO3'].unique():
#
#    if  code  not in injuries_fatal['ISO3'].unique():
#        region_broad = correspondance.loc[(correspondance['ISO3 Code']==code),'ILO Subregion - Broad'].to_string(index=False,header=False)
#        region = correspondance.loc[(correspondance['ISO3 Code']==code),'ILO Region'].to_string(index=False,header=False)
#        if region_broad  in Sub_region_name :
#            injuries_split_ROW_drop.loc[injuries_split_ROW_drop['ISO3']==code] = injuries_split_ROW_drop.loc[injuries_split_ROW_drop['ISO3']==code][injuries_split_ROW_drop["estimate"].str.contains("Upper") == False]
#            #injuries_split_ROW_drop = injuries_split_ROW_drop[injuries_split_ROW_drop["estimate"].str.contains("Lower") == False]
#        #elif region in region_name:
#            #injuries_split_ROW_drop = injuries_split_ROW_drop[injuries_split_ROW_drop["estimate"].str.contains("Upper") == False]
#            #injuries_split_ROW_drop = injuries_split_ROW_drop[injuries_split_ROW_drop["estimate"].str.contains("Lower") == False]
#    #else:
#     #   print(code)
#      #  injuries_split_ROW_drop = inju
#injuries_split_ROW_drop.to_csv('clean_death_ISO3_2004.csv',index=False)

ISO_code = list(injuries_split_ROW['ISO3'])
injuries_split_ROW.insert(1, 'EXIO3', cc_all.convert(names = ISO_code, to='EXIO3'))
injuries_split_EXIO3=injuries_split_ROW.copy()

injuries_split_EXIO3_aggregate=injuries_split_EXIO3.groupby(['EXIO3','EXIOBASE sector','sex','estimate','year'],axis=0).sum()
injuries_split_EXIO3_aggregate=injuries_split_EXIO3_aggregate.drop(columns='ISO3')
injuries_split_EXIO3_aggregate.to_csv("death_per_EXIO3.csv")

