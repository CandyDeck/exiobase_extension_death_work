from selenium import webdriver
from selenium.webdriver.common.by import By

from time import sleep

def url_who():
    #url = 'https://who-ilo-joint-estimates.shinyapps.io/OccupationalBurdenOfDisease/'
    
    profile = webdriver.FirefoxProfile()

    profile.set_preference("browser.download.folderList", 2)
    
    profile.set_preference("browser.download.manager.showWhenStarting", False)
    
    #profile.set_preference("browser.download.dir", os.getcwd() + "\\" + str(today.strftime('%Y%m%d')) + "\\"  )
    
    #profile.set_preference("browser.helperApps.neverAsk.saveToDisk", "application/x-gzip")
    
    driver = webdriver.Firefox(firefox_profile=profile)
    
     

    driver.get('https://who-ilo-joint-estimates.shinyapps.io/OccupationalBurdenOfDisease/')

    driver.implicitly_wait(60) # wait until page will be loaded

 


    # driver = webdriver.Firefox()
    # driver.maximize_window()
    # driver.get(url)
    # sleep(30)
    
    
    driver.find_element(By.XPATH, "/html/body/nav/div/div[2]/ul/li[3]/a").click()

    #driver.find_element_by_xpath("/html/body/nav/div/div[2]/ul/li[3]/a").click()
    sleep(30)
    
    #driver.find_element_by_xpath("/html/body/div[1]/div/div[3]/div/div[1]/div/button/div/div/div").click()
    driver.find_element(By.XPATH, "/html/body/div[1]/div/div[3]/div/div[1]/div/button/div/div/div").click()

    driver.find_element(By.XPATH, "/html/body/div[1]/div/div[3]/div/div[1]/div/div/div/ul/li[17]/a/span[2]").click()
    #driver.find_element_by_xpath("/html/body/div[1]/div/div[3]/div/div[1]/div/div/div/ul/li[17]/a/span[2]").click()    
    sleep(30)
    
    url_table = driver.find_element_by_id("cmd_export_data").get_attribute('href')
    #driver.find_element_by_id("cmd_export_data").click()
    #driver.close()
    #driver.quit()
    return url_table


def url_who_non_fatal():
    url = 'https://who-ilo-joint-estimates.shinyapps.io/OccupationalBurdenOfDisease/'
    driver = webdriver.Firefox()
    driver.maximize_window()
    driver.get(url)
    sleep(30)
    
    driver.find_element_by_xpath("/html/body/nav/div/div[2]/ul/li[3]/a").click()
    sleep(30)
    
    driver.find_element_by_xpath("/html/body/div[1]/div/div[3]/div/div[1]/div/button/div/div/div").click()
    driver.find_element_by_xpath("/html/body/div[1]/div/div[3]/div/div[1]/div/div/div/ul/li[17]/a/span[2]").click()    
    sleep(30)
    
    url_table = driver.find_element_by_id("cmd_export_data").get_attribute('href')
    #driver.find_element_by_id("cmd_export_data").click()
    
    #driver.close()
    #driver.quit()
    return url_table