import pandas as pd

workforce_exio3 = pd.read_csv('table_workforce_by_EXIO3.csv')
workforce_exio3 = workforce_exio3[workforce_exio3["classif1"].str.contains("DETAIL") == True]
workforce_exio_aggregate = workforce_exio3.groupby(['EXIO3','sex','classif1','time']).sum()

concordance = pd.read_excel('Exiobase_ISIC_Rev-4.xlsx','ILO_mapping_sector')

death_cat = pd.read_csv('injuries_per_EXIO3.csv')
death_cat=death_cat[death_cat.estimate == 'Upper']

classif_detail = [s for s in workforce_exio3['classif1'].unique() if("DETAILS" in s and "TOTAL" not in s)]
fatal_exio3= pd.read_csv('fatal_injuries_exio3_300323.csv')


columns_death_split = ['EXIO3','Sector','Mapping','Sex','Year','Value']
death_split2 = pd.DataFrame(columns=columns_death_split)
death_split=pd.read_csv('death_split_0104.csv')

for code in workforce_exio3['EXIO3'].unique():
    if code == 'GB' :
        print(code)
        for year in range(2009,2018):
            print(year)
            split_year = pd.read_excel('split.xlsx',str(year))
            for a in classif_detail :
                print(a)

                list_name = concordance.loc[concordance['ISIC REV 4_ILO_Alteryx']==a,['Name']]
                for b in list_name['Name']:
                    print(b)
                    for c in workforce_exio3['sex'].unique():
                        print(c)
                        if c=='SEX_F':
                            pop_F = float(workforce_exio3.loc[(workforce_exio3['EXIO3']==code)&(workforce_exio3['sex']=='SEX_F')&(workforce_exio3['classif1']==a)&(workforce_exio3['time']==year),['obs_value']].to_string(header=False, index=False))                  
                            pop_M = float(workforce_exio3.loc[(workforce_exio3['EXIO3']==code)&(workforce_exio3['sex']=='SEX_M')&(workforce_exio3['classif1']==a)&(workforce_exio3['time']==year),['obs_value']].to_string(header=False, index=False))                  
                            ratio_F =  float(pop_F / (pop_F + pop_M)) 
                            ratio_M =  float(pop_M / (pop_F + pop_M))  
                            print(death_split.loc[(death_split['EXIO3']==code)&(death_split['Sector']==b)&(death_split['Mapping']==a)&(death_split['Sex']=='SEX_F')&(death_split['Year']==year),'Value'].to_string(header=False, index=False))
                            old_value = float(death_split.loc[(death_split['EXIO3']==code)&(death_split['Sector']==b)&(death_split['Mapping']==a)&(death_split['Sex']=='SEX_F')&(death_split['Year']==year),'Value'].to_string(header=False, index=False))
                            new_value = float(old_value * ratio_F)
                            d={'EXIO3':[code],'Sector': [b],'Mapping':[a],'Sex':[c],'Year':[year],'Value':[new_value]}
                            df = pd.DataFrame(data=d)
                            death_split2=pd.concat([death_split2, df])
                            
                            #print(old_value,new_value)
                            #print(death_split.loc[(death_split['EXIO3']==code)&(death_split['Sector']==b)&(death_split['Mapping']==a)&(death_split['Sex']=='SEX_F')&(death_split['Year']==year),'Value'])
                            #print(new_value)
                            #death_split.iloc[(death_split['EXIO3']==code)&(death_split['Sector']==b)&(death_split['Mapping']==a)&(death_split['Sex']=='SEX_F')&(death_split['Year']==year),'Value']= death_split.loc[(death_split['EXIO3']==code)&(death_split['Sector']==b)&(death_split['Mapping']==a)&(death_split['Sex']=='SEX_F')&(death_split['Year']==year),'Value'].replace([])new_value 
                            #print(death_split.loc[(death_split['EXIO3']==code)&(death_split['Sector']==b)&(death_split['Mapping']==a)&(death_split['Sex']=='SEX_F')&(death_split['Year']==year),'Value'])

                            #death_split.loc[(death_split['EXIO3']==code)&(death_split['Sector']==b)&(death_split['Mapping']==a)&(death_split['Sex']=='SEX_F')&(death_split['Year']==year),'NEW Value'] = new_value
                        elif c=='SEX_M':
                            pop_F = float(workforce_exio3.loc[(workforce_exio3['EXIO3']==code)&(workforce_exio3['sex']=='SEX_F')&(workforce_exio3['classif1']==a)&(workforce_exio3['time']==year),['obs_value']].to_string(header=False, index=False))                  
                            pop_M = float(workforce_exio3.loc[(workforce_exio3['EXIO3']==code)&(workforce_exio3['sex']=='SEX_M')&(workforce_exio3['classif1']==a)&(workforce_exio3['time']==year),['obs_value']].to_string(header=False, index=False))                  
                            ratio_F =  float(pop_F / (pop_F + pop_M)) 
                            ratio_M =  float(pop_M / (pop_F + pop_M))           
                            old_value = float(death_split.loc[(death_split['EXIO3']==code)&(death_split['Sector']==b)&(death_split['Mapping']==a)&(death_split['Sex']=='SEX_M')&(death_split['Year']==year),'Value'].to_string(header=False, index=False))
                            new_value = float(old_value * ratio_M)
                            d={'EXIO3':[code],'Sector': [b],'Mapping':[a],'Sex':[c],'Year':[year],'Value':[new_value]}
                            df = pd.DataFrame(data=d)
                            death_split2=pd.concat([death_split2, df])
            death_split2.to_csv('death_split_0104_updated_values3.csv',index=False)       
            #print(death_split)
            
                    
                            