import pandas as pd
import country_converter as coco


workforce_exio3 = pd.read_csv('table_workforce_by_EXIO3.csv')
workforce_exio3 = workforce_exio3[workforce_exio3["classif1"].str.contains("DETAIL") == True]


workforce_exio_aggregate = workforce_exio3.groupby(['EXIO3','sex','classif1','time']).sum()

concordance = pd.read_excel('Exiobase_ISIC_Rev-4.xlsx','ILO_mapping_sector')

death_cat = pd.read_csv('injuries_per_EXIO3.csv')
death_cat=death_cat[death_cat.estimate == 'Upper']

#columns_death_split = ['EXIO3','Sector','Mapping','Sex','Year','Value']
#death_split = pd.DataFrame(columns=columns_death_split)
classif_detail = [s for s in workforce_exio3['classif1'].unique() if("DETAILS" in s and "TOTAL" not in s)]
fatal_exio3= pd.read_csv('fatal_injuries_exio3_300323.csv')

death_split=pd.read_csv('death_split_0104.csv')


for code in workforce_exio3['EXIO3'].unique():
    if (code == 'IT' or code == 'JP' or code == 'KR' or code == 'LT' or code == 'LU' or code == 'LV' or code == 'MT' or code == 'MX' or code == 'NL' or code == 'NO' or code == 'PL' or code == 'PT' or code == 'RO' or code == 'RU' or code == 'SE' or code == 'SI' or code == 'SK' or code == 'TR' or code == 'TW' or code == 'US' or code == 'WA' or code == 'WE' or code == 'WF' or code == 'WL' or code == 'WM' or code == 'ZA'  ):
        print(code)
        for year in range(2009,2018):
            print(year)
            split_year = pd.read_excel('split.xlsx',str(year))
            for a in classif_detail :
                #print(code, a)
                list_name = concordance.loc[concordance['ISIC REV 4_ILO_Alteryx']==a,['Name']]
                for b in list_name['Name']:
                    for c in workforce_exio3['sex'].unique():
                        if c=='SEX_F':
                            pop_sex_mapping=float(workforce_exio_aggregate.loc[code, c, a,year].to_string(header=False, index=False))
                            pop_sex_mapping = 1000*pop_sex_mapping
                            death_sex_sector=float(fatal_exio3.loc[(fatal_exio3['EXIO3']==code)&(fatal_exio3['EXIOBASE sector']==a)&(fatal_exio3['sex']=='Female')&(fatal_exio3['year']==year),['death']].to_string(header=False, index=False))                        

                            pop_sex_sector=float(split_year.loc[(split_year['Country']==code)&(split_year['Sector']==b)&(split_year['Mapping']==a),['Female']].to_string(header=False, index=False))                        
                            number_of_death= death_sex_sector * pop_sex_sector/pop_sex_mapping

                            d={'EXIO3':[code],'Sector': [b],'Mapping':[a],'Sex':[c],'Year':[year],'Value':[number_of_death]}
                            df = pd.DataFrame(data=d)
                            death_split=pd.concat([death_split, df])
                        elif c=='SEX_M' :
                            pop_sex_mapping=float(workforce_exio_aggregate.loc[code, c, a,year].to_string(header=False, index=False))
                            pop_sex_mapping = 1000*pop_sex_mapping
                            death_sex_sector=float(fatal_exio3.loc[(fatal_exio3['EXIO3']==code)&(fatal_exio3['EXIOBASE sector']==a)&(fatal_exio3['sex']=='Male')&(fatal_exio3['year']==year),['death']].to_string(header=False, index=False))                        

                            pop_sex_sector=float(split_year.loc[(split_year['Country']==code)&(split_year['Sector']==b)&(split_year['Mapping']==a),['Male']].to_string(header=False, index=False))                        
                            number_of_death= death_sex_sector * pop_sex_sector/pop_sex_mapping

                            d={'EXIO3':[code],'Sector': [b],'Mapping':[a],'Sex':[c],'Year':[year],'Value':[number_of_death]}
                            df = pd.DataFrame(data=d)
                            death_split=pd.concat([death_split, df])

                        death_split.to_csv('death_split_0104.csv',index=False)       


